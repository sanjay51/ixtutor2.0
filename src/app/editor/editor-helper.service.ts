import { EditorOptionsComponent } from './editor-options/editor-options.component';
import { Observable } from 'rxjs/Observable';
import { CoursesService } from './../shared/courses.service';
import { Course, Instruction, Section, Chapter } from './../course';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class EditorHelperService {
    activeEditor: AbstractEditor;
    editorUI: EditorUI;
    instructionsEditorUI: InstructionsEditorUI;

    constructor(private router: Router, private coursesService: CoursesService) { }

    save(editorOptionsUI: EditorOptionsUI) {
        editorOptionsUI.setSavingMode();
        console.log(this.activeEditor);
        this.activeEditor.save()
            .subscribe(
            response => {
                editorOptionsUI.setSuccessMode();
                this.coursesService.putInCache(this.activeEditor.getCourse());
                this.editorUI.setCourse(this.activeEditor.getCourse());
            },
            error => {
                editorOptionsUI.setErrorMode();
            }
            )

    }

    navigateToReadMode() {
        // http://localhost:3000/course/DesignprinciplesinJava-593/chapter/0/section/1
        let link = ['course', this.activeEditor.getCourse().id,
            'chapter', this.activeEditor.getChapter().id,
            'section', this.activeEditor.getSection().id];
        this.router.navigate(link);
    }

    setActiveEditor(editor: AbstractEditor) {
        this.activeEditor = editor;
    }

    setEditorUI(editorUI: EditorUI) {
        this.editorUI = editorUI;
    }

    setInstructionsEditorUI(instructionsEditorUI: InstructionsEditorUI) {
        this.instructionsEditorUI = instructionsEditorUI;
    }

    saveCourseMetadata(course: Course): Observable<any> {
        return this.coursesService.saveCourse(course);
    }

    saveChapterMetadata(course: Course): Observable<any> {
        return this.coursesService.saveCourseChapters(course);
    }

    saveSection(course: Course): Observable<any> {
        return this.coursesService.saveCourseChapters(course);
    }

    showInstructionEditorWidget(instructions: Instruction) {
        this.instructionsEditorUI.show(instructions);
    }

}

export interface AbstractEditor {
    save(): Observable<any>;
    getCourse(): Course;
    getChapter(): Chapter;
    getSection(): Section;
}

export interface EditorOptionsUI {
    setSuccessMode(): any;
    setErrorMode(): any;
    setSavingMode(): any;
}

export interface EditorUI {
    setCourse(course: Course): any;
}

export interface InstructionsEditorUI {
    show(instructions: Instruction): any;
}