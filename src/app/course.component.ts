import { UIInteractionService } from './shared/ui-interaction.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Course, Section, Policy, Chapter, Exercise } from './course';
import { CoursesService } from './shared/courses.service';
import { RuleEvaluatorService } from './rule-evaluator.service';
import { MarkupHelperService } from './shared/markup-helper.service';
import { AuthenticationService } from './shared/authentication.service';

@Component({
    selector: 'course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, OnDestroy {
    courseId: string;
    chapterId: number;
    sectionId: number;

    course: Course;
    chapter: Chapter;
    section: Section;

    userScore: UserScore = new UserScore();

    // two way bound members
    userInput: string;
    output: string;
    isCompleted: boolean = false; // has user completed current section

    constructor(private route: ActivatedRoute, private ruleEvaluatorService: RuleEvaluatorService,
        private coursesService: CoursesService, private router: Router,
        private markupHelperService: MarkupHelperService,
        private uiInteractionService: UIInteractionService,
        private authenticationService: AuthenticationService) { }

    ngOnInit(): void {
        this.uiInteractionService.hideFooter();
        this.route.params.forEach((params: Params) => {
            this.courseId = params['id'];
            this.chapterId = +params['chapter'];
            this.sectionId = +params['section'];

            /* Don't reload course if already loaded.
             * ngOnInit() is called everytime the route changes,
             * but the course object only need to be initialized once, so
             * no new network call is needed on chapter or section change.
             */
            
            if (this.course) {
                this.initCourse(this.course);
                return;
            }

            this.coursesService.getCourse(this.courseId)
                .subscribe(
                course => this.initCourse(course)
                );
        })
    }

    ngOnDestroy(): void {
        this.uiInteractionService.unhideFooter();
    }

    initCourse(course: Course) {
        this.course = course;
        this.chapter = this.course.chapters[this.chapterId];
        this.section = this.chapter.sections[this.sectionId];
    }

    getInstructionText() {
        return this.markupHelperService.parseToHTML(this.section.instruction.text);
    }

    evaluate(): void {
        for (let text of this.getSection().validInputs) {
            let policy: Policy = this.getSection().getPolicy();
            let isMatch: boolean = this.ruleEvaluatorService.evaluate(this.userInput, text, policy);

            if (isMatch) {
                this.markSectionAsCompleted();
                break;
            } else {
                this.output = this.getSection().output.text + "Please check again.";
            }
        }
    }

    getSection(): Section {
        return this.course.chapters[this.chapterId].sections[this.sectionId];
    }

    getInputPlaceholder(): string {
        return this.getSection().inputPlaceholder;
    }

    markSectionAsCompleted() {
        this.isCompleted = true;
        this.output = this.getSection().output.text + "Hit Next to continue.";
    }

    gotoNextSection(): void {
        let newChapterId: number = this.chapterId;
        let newSectionId: number = this.sectionId;

        if (this.sectionId >= this.course.getMaxSectionId(this.chapterId)) {
            if (this.chapterId >= this.course.getMaxChapterId()) {
                // Already at the end of course. Do nothing.
                // TODO: This should show end of course page later.
                return;
            }

            // Goto 1st section of next chapter
            newSectionId = 0;
            newChapterId = this.chapterId + 1;
        } else {
            // Goto next section of same chapter
            newSectionId = this.sectionId + 1;
        }

        this.isCompleted = false;
        let link = ['/course', this.course.id, 'chapter', newChapterId, 'section', newSectionId];
        this.router.navigate(link);
    }

    gotoPreviousSection(): void {
        let newChapterId: number = this.chapterId;
        let newSectionId: number = this.sectionId;

        if (this.sectionId <= this.course.getMinSectionId(this.chapterId)) {
            if (this.chapterId <= this.course.getMinChapterId()) {
                // Already at the beginning of course. Do nothing.
                // TODO: Disable back or previous button on UI
                return;
            }

            // Goto final section of previous chapter
            newChapterId = this.chapterId - 1;
            newSectionId = this.course.getMaxSectionId(newChapterId);
        } else {
            // Goto previous section of same chapter
            newSectionId = newSectionId - 1;
        }

        let link = ['/course', this.course.id, 'chapter', newChapterId, 'section', newSectionId];
        this.router.navigate(link);
    }

    loadChapter(chapter: Chapter) {
        let newChapterId: number = chapter.id;
        let newSectionId: number = chapter.meta.beginSectionIndex;

        let link = ['/course', this.course.id, 'chapter', newChapterId, 'section', newSectionId];
        this.router.navigate(link);
    }

    loadSection(section: Section) {
        let newSectionId: number = section.id;
        let link = ['/course', this.course.id, 'chapter', this.chapterId, 'section', newSectionId];
        this.router.navigate(link);
    }

    editSection(section: Section) {
        // http://localhost:3000/editor/course/DesignprinciplesinJava-593/chapter/0/section/0
        let link = ['/editor', 
            'course', this.course.id, 
            'chapter', this.chapter.id,
            'section', section.id];
        this.router.navigate(link);
    }

    isCourseEditor(course: Course) {
        return this.authenticationService.isCourseEditor(course);
    }

    evaluateExercise(exercise: Exercise, answer: number) {
        if (exercise.isCorrectOption(answer)) {
            this.userScore.put(this.courseId,
                this.chapterId,
                this.sectionId,
                exercise.id,
                exercise.points);
            this.checkIfScoreAchieved();
        }
    }

    checkIfScoreAchieved() {
        if (this.userScore.get(this.courseId, this.chapterId, this.sectionId) >= 3) {
            // pass
            this.isCompleted = true;
        }
    }

    getUserScore() {
        return this.userScore.get(this.courseId, this.chapterId, this.sectionId);
    }
}

export class UserScore {
    userScore = {}

    put(courseId: string, chapterId: number, sectionId: number, exerciseId: number, points: number) {
        if (this.userScore[courseId] == undefined) {
            this.userScore[courseId] = {}
        }

        if (this.userScore[courseId][sectionId] == undefined) {
            this.userScore[courseId][sectionId] = {}
        }

        if (this.userScore[courseId][sectionId][exerciseId] == undefined) {
            this.userScore[courseId][sectionId][exerciseId] = {
                score: 0
            }
        }
        
        this.userScore[courseId][sectionId][exerciseId].score = +points
    }

    get(courseId: string, chapterId: number, sectionId: number): number {
        if (this.userScore[courseId] == undefined
            || this.userScore[courseId][sectionId] == undefined) {
                return 0;
        }

        let score = 0;
        for (let exerciseLevelScore in this.userScore[courseId][sectionId]) {
            score += this.userScore[courseId][sectionId][exerciseLevelScore].score;
        }

        return score;
    }

}