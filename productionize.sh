echo '\n//shim.min.js' > src/sanjay.js; 
cat node_modules/core-js/client/shim.min.js >> src/sanjay.js;

echo '\n//build.js' >> src/sanjay.js;
cat src/build.js >> src/sanjay.js;
    
echo '\n//zone.js\n' >> src/sanjay.js; 
cat node_modules/zone.js/dist/zone.js >> src/sanjay.js;
    
echo '\n//system.src.js\n' >> src/sanjay.js;
cat node_modules/systemjs/dist/system.src.js >> src/sanjay.js;

cat src/index.html > prod.html

echo >> prod.html
echo "<style>" >> prod.html
echo >> prod.html
cat src/styles.css >> prod.html
echo >> prod.html
echo "</style>" >> prod.html

echo >> prod.html
echo "<style>" >> prod.html
echo >> prod.html
cat node_modules/@angular/material/prebuilt-themes/indigo-pink.css >> prod.html
echo >> prod.html
echo "</style>" >> prod.html

echo >> prod.html
echo "<script>" >> prod.html
echo >> prod.html
cat node_modules/core-js/client/shim.min.js >> prod.html
echo >> prod.html
echo "</script>" >> prod.html

echo >> prod.html
echo "<script>" >> prod.html
echo >> prod.html
cat node_modules/zone.js/dist/zone.js >> prod.html
echo >> prod.html
echo "</script>" >> prod.html

echo >> prod.html
echo "<script>" >> prod.html
echo >> prod.html
cat node_modules/systemjs/dist/system.src.js >> prod.html
echo >> prod.html
echo "</script>" >> prod.html


